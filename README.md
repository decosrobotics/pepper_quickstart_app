# Pepper QuickStart APP

This is a simple APP to teach you how to code for Pepper using Choregraphe.   
The APP is very simple. Just get close to Pepper and it will try to predict your gender and age. It will also count the number of people that interacted with it so far.

The idea is to teach you some basic principles like how to manipulate memory keys and share them with the tablet, as well as how to use the Dialog and play animations.

## Requirements

1. Pepper Robot with naoqi 2.5 or higher
2. Choregraphe version 2.5 or higher
3. Text editor to edit the html files. Google Chrome is also advised

Required knowledge:
1. Python
2. HTML, CSS and Javascript

## Coding for Pepper

1. Make sure that you are in the same network as Pepper.
2. Make sure that you know the IP of the robot (press the button on the chest - behind the tablet once and Pepper will say its IP address). `pepper.local` usually works.
3. Connect to Pepper using Choregraphe
4. Open this project (pepper_quickstart_sample.pml) on Choregraphe
5. Take a look at the code (both Choregraphe and HTML)
6. Upload the code to the robot

## Useful Links
1. [Download Choregraphe](https://community.ald.softbankrobotics.com/en/resources/software/language/en-gb) You will need a Softbank-Aldebaran account. You can create it in the URL as well.  
2. Use this memory key during the installation `654e-4564-153c-6518-2f44-7562-206e-4c60-5f47-5f45`.
3. [NAOqi Documentation](http://doc.aldebaran.com/2-5/naoqi/)
