<?xml version="1.0" encoding="UTF-8" ?>
<Package name="pepper_quickstart_sample" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="saxophone" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="elephant" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="behavior_1" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs>
        <Dialog name="Chitchat" src="Chitchat/Chitchat.dlg" />
    </Dialogs>
    <Resources>
        <File name="jquery" src="html/includes/jquery.js" />
        <File name="LICENSE" src="html/includes/materialize/LICENSE" />
        <File name="README" src="html/includes/materialize/README.md" />
        <File name="materialize" src="html/includes/materialize/css/materialize.css" />
        <File name="materialize.min" src="html/includes/materialize/css/materialize.min.css" />
        <File name="Roboto-Bold" src="html/includes/materialize/fonts/roboto/Roboto-Bold.eot" />
        <File name="Roboto-Bold" src="html/includes/materialize/fonts/roboto/Roboto-Bold.ttf" />
        <File name="Roboto-Bold" src="html/includes/materialize/fonts/roboto/Roboto-Bold.woff" />
        <File name="Roboto-Bold" src="html/includes/materialize/fonts/roboto/Roboto-Bold.woff2" />
        <File name="Roboto-Light" src="html/includes/materialize/fonts/roboto/Roboto-Light.eot" />
        <File name="Roboto-Light" src="html/includes/materialize/fonts/roboto/Roboto-Light.ttf" />
        <File name="Roboto-Light" src="html/includes/materialize/fonts/roboto/Roboto-Light.woff" />
        <File name="Roboto-Light" src="html/includes/materialize/fonts/roboto/Roboto-Light.woff2" />
        <File name="Roboto-Medium" src="html/includes/materialize/fonts/roboto/Roboto-Medium.eot" />
        <File name="Roboto-Medium" src="html/includes/materialize/fonts/roboto/Roboto-Medium.ttf" />
        <File name="Roboto-Medium" src="html/includes/materialize/fonts/roboto/Roboto-Medium.woff" />
        <File name="Roboto-Medium" src="html/includes/materialize/fonts/roboto/Roboto-Medium.woff2" />
        <File name="Roboto-Regular" src="html/includes/materialize/fonts/roboto/Roboto-Regular.eot" />
        <File name="Roboto-Regular" src="html/includes/materialize/fonts/roboto/Roboto-Regular.ttf" />
        <File name="Roboto-Regular" src="html/includes/materialize/fonts/roboto/Roboto-Regular.woff" />
        <File name="Roboto-Regular" src="html/includes/materialize/fonts/roboto/Roboto-Regular.woff2" />
        <File name="Roboto-Thin" src="html/includes/materialize/fonts/roboto/Roboto-Thin.eot" />
        <File name="Roboto-Thin" src="html/includes/materialize/fonts/roboto/Roboto-Thin.ttf" />
        <File name="Roboto-Thin" src="html/includes/materialize/fonts/roboto/Roboto-Thin.woff" />
        <File name="Roboto-Thin" src="html/includes/materialize/fonts/roboto/Roboto-Thin.woff2" />
        <File name="materialize" src="html/includes/materialize/js/materialize.js" />
        <File name="materialize.min" src="html/includes/materialize/js/materialize.min.js" />
        <File name="qimessaging" src="html/includes/qimessaging.js" />
        <File name="index" src="html/index.html" />
        <File name="welbo-logo" src="html/resources/welbo-logo.png" />
        <File name="style" src="html/style.css" />
        <File name="elephant" src="html/resources/elephant.png" />
        <File name="laughing" src="html/resources/laughing.png" />
        <File name="chewbacca" src="html/resources/chewbacca.jpg" />
        <File name="epicsax" src="saxophone/epicsax.ogg" />
        <File name="elephant" src="elephant/elephant.ogg" />
        <File name="saxophone" src="html/resources/saxophone.png" />
        <File name="chewbacca_saxophone" src="html/resources/chewbacca_saxophone.png" />
        <File name="README" src="README.md" />
        <File name="DPA_logo_small" src="html/DPA_logo_small.png" />
        <File name="Logo KennisEnK_White_" src="html/Logo KennisEnK_White_.png" />
        <File name="decos_logo_small" src="html/decos_logo_small.png" />
    </Resources>
    <Topics>
        <Topic name="Chitchat_enu" src="Chitchat/Chitchat_enu.top" topicName="Chitchat" language="en_US" />
        <Topic name="Chitchat_dun" src="Chitchat/Chitchat_dun.top" topicName="Chitchat" language="nl_NL" />
    </Topics>
    <IgnoredPaths />
</Package>
